import { Component, OnInit } from '@angular/core';
import { Student } from '../student';
import { StudentService } from '../student.service';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {

  student:Student;

  constructor(private studentService:StudentService) { }

  ngOnInit(): void {
    this.student = {name:null,mathematics:null,psychometric:null,pay:false}
  }

  // predict(){

  //       this.studentService.predict(this.student).subscribe(
  //         res => {console.log(res);
  //           if(res > 0.5){
  //             var result = 'Wont drop';
  //           } else {
  //             var result = 'Drop'
  //           }
  //       }
  //     );  
  //   }

}
