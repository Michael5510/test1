export interface Student {
    name:string;
    mathematics:number;
    psychometric:number;
    pay:boolean;
}
