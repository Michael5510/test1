// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyC_TA7NLDyQkNXSqDpARbankeSmMHNSnEI",
    authDomain: "test-9c6d7.firebaseapp.com",
    projectId: "test-9c6d7",
    storageBucket: "test-9c6d7.appspot.com",
    messagingSenderId: "540132504821",
    appId: "1:540132504821:web:ff28eff8da6c5a1c7201bc"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
